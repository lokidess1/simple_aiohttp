from aiohttp import web
import asyncio


async def send_mail():
    await asyncio.sleep(10)
    print("Email sent")


async def home(request):
    # asyncio.create_task(send_mail()) 3.7+
    asyncio.ensure_future(send_mail())
    return web.json_response({"message": "Hello world"})


class SomeMultiClass(web.View):

    async def get(self):
        return web.json_response({"message": "I'm get!"})

    async def post(self):
        return web.json_response({"message": "I'm post!"})
