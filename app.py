from aiohttp import web
from handlers import home, SomeMultiClass


def create_app():
    app = web.Application()
    app.router.add_get('/', home)
    app.router.add_route('*', "/simple/", SomeMultiClass)

    return app


if __name__ == '__main__':
    app = create_app()
    web.run_app(
        app=app,
        port=5000
    )
